const path = require("path");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = (env, options) => {
  if (options.mode == "development" || "production") {
    return {
      entry: {
        // main: "./assets/js/main.js",
        style: "./assets/scss/main.scss",
        admin: "./assets/scss/admin.scss",
        login: "./assets/scss/login.scss"
      },
      output: {
        path: path.resolve(__dirname, "assets")
      },
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
              loader: "babel-loader",
              options: {
                presets: ["babel-preset-env"]
              }
            }
          },
          {
            test: /\.(sa|sc|c)ss$/,
            use: [
              { loader: MiniCssExtractPlugin.loader },
              "css-loader",
              "sass-loader"
            ]
          }
        ]
      },
      plugins: [
        // Adding our UglifyJS plugin
        new UglifyJSPlugin(),
        new MiniCssExtractPlugin({
          filename: "css/[name].css",
          chunkFilename: "[id].css"
        })
      ]
    };
  }
};
